;;; guile-fontconfig --- FFI bindings for FontConfig
;;; Copyright © 2021 Nicolò Balzarotti <nicolo@nixo.xyz>
;;;
;;; This file is part of guile-fontconfig.
;;;
;;; Guile-fontconfig is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU Lesser General Public License
;;; as published by the Free Software Foundation; either version 3 of
;;; the License, or (at your option) any later version.
;;;
;;; Guile-fontconifg is distributed in the hope that it will be
;;; useful, but WITHOUT ANY WARRANTY; without even the implied
;;; warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
;;; See the GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU Lesser General Public
;;; License along with guile-fontconfig.  If not, see
;;; <http://www.gnu.org/licenses/>.

;;; Code:

(define-module (fontconfig)
  #:use-module (fontconfig font-set)
  #:use-module (fontconfig globals)
  #:use-module (fontconfig pattern)
  #:use-module (fontconfig object-set)
  #:re-export (fontconfig-version
               *fontconfig*
               font-list
               list->font-set font-set->list
               make-pattern copy-pattern pattern-filter
               pattern=? pattern-subset=
               pattern-get string->pattern pattern->string pattern->format
               pattern-substitute pattern-substitute! render-prepare
               font-match font-set-match font-sort font-set-sort
               make-object-set object-set))
