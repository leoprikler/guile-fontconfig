#!/bin/sh
exec guile -e main -s "$0" "$@"
!#

(use-modules (fontconfig)
             ((fontconfig debug) #:prefix debug:)
             (ice-9 getopt-long)
             (ice-9 match)
             (ice-9 optargs)
             (srfi srfi-26))

(define *grammar*
  '((version (single-char #\V) (value #f))
    (help    (single-char #\h) (value #f))
    (config  (single-char #\c) (value #f))
    (default (single-char #\d) (value #f))
    (format  (single-char #\f) (value #t))))

(define (do-pattern pattern os format subst)
  (apply pattern-substitute! pattern subst)
  (let ((pattern (pattern-filter pattern os)))
    (if format
        (display (pattern->format pattern format))
        (debug:print-pattern pattern))))

(define (main args)
  (let* ((options (getopt-long args *grammar*))
         (option (cute option-ref options <> <>))
         (option? (cute option <> #f))
         (args
          `(#:config ,(and (option? 'config) 'pattern)
            #:default ,(option? 'default)))
         (format
          (option 'format #f)))
    (cond
     ((option? 'help)
      (display "\
usage: pattern.scm [OPTION...] [PATTERN [ELEMENT ...]]
List best font matching PATTERN

Options:
  -c, --config         perform config substitution on PATTERN
  -d, --default        perform default substitution on PATTERN
  -f, --format=FORMAT  use the given output format
  -V, --version        display font config version and exit
  -h, --help           display this help and exit")
      (newline))
     ((option? 'version)
      (display "guile-fontconfig v0.1")
      (newline))
     (else
      (match (option '() '())
        (() (do-pattern (make-pattern) #f format args))
        ((pattern) (do-pattern (string->pattern pattern)
                               #f format args))
        ((pattern . elements)
         (do-pattern (string->pattern pattern)
                     (apply make-object-set (map string->symbol elements))
                     format args)))))))
