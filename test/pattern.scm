;;; guile-fontconfig --- FFI bindings for FontConfig
;;; Copyright © 2021 Nicolò Balzarotti <nicolo@nixo.xyz>
;;; Copyright © 2021 Leo Prikler <leo.prikler@student.tugraz.at>
;;;
;;; This file is part of guile-fontconfig.
;;;
;;; Guile-fontconfig is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU Lesser General Public License
;;; as published by the Free Software Foundation; either version 3 of
;;; the License, or (at your option) any later version.
;;;
;;; Guile-fontconfig is distributed in the hope that it will be
;;; useful, but WITHOUT ANY WARRANTY; without even the implied
;;; warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
;;; See the GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU Lesser General Public
;;; License along with guile-fontconfig.  If not, see
;;; <http://www.gnu.org/licenses/>.

(use-modules (fontconfig)
             ((fontconfig pattern) #:select (unwrap-pattern))
             (srfi srfi-64))

(test-begin "pattern")

(test-assert "eq? implies pattern=?"
  (let ((pattern (make-pattern)))
    (pattern=? pattern pattern)))

(test-assert "pattern=? all substituted"
  (pattern=? (pattern-substitute (make-pattern))
             (pattern-substitute (make-pattern))))

(test-assert "pattern=? copied pattern"
  (let* ((pattern (pattern-substitute (make-pattern)))
         (copy (copy-pattern pattern)))
    (and (not (eq? (unwrap-pattern pattern)
                   (unwrap-pattern copy)))
         (pattern=? pattern copy))))

(test-assert "not pattern=?, size differs"
  (not (pattern=? (pattern-substitute (make-pattern))
                  (pattern-substitute (make-pattern #:size 18)))))

(test-assert "pattern-subset="
  (let ((pattern-family=? (pattern-subset= (object-set family)))
        (p1 (make-pattern #:family "i love you, you love me" #:size 12))
        (p2 (make-pattern #:family "i love you, you love me" #:size 18)))
    (and
     (pattern-family=? p1 p2)
     (not ((pattern-subset= (object-set size)) p1 p2)))))

(test-end "pattern")
