;;; guile-fontconfig --- FFI bindings for FontConfig
;;; Copyright © 2021 Nicolò Balzarotti <nicolo@nixo.xyz>
;;; Copyright © 2021 Leo Prikler <leo.prikler@student.tugraz.at>
;;;
;;; This file is part of guile-fontconfig.
;;;
;;; Guile-fontconfig is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU Lesser General Public License
;;; as published by the Free Software Foundation; either version 3 of
;;; the License, or (at your option) any later version.
;;;
;;; Guile-fontconfig is distributed in the hope that it will be
;;; useful, but WITHOUT ANY WARRANTY; without even the implied
;;; warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
;;; See the GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU Lesser General Public
;;; License along with guile-fontconfig.  If not, see
;;; <http://www.gnu.org/licenses/>.

;;; Commentary:
;;
;; Low-level FFI bindings.
;;
;;; Code:

(define-module (fontconfig bindings)
  #:use-module (fontconfig config)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-26)
  #:use-module (system foreign))

(define fontconfig-func
  (let ((lib (dynamic-link %libfontconfig)))
    (lambda (return-type function-name arg-types)
      "Return a procedure for the foreign function FUNCTION-NAME in
the FontConfig shared library.  That function must return a value of
RETURN-TYPE and accept arguments of ARG-TYPES."
      (pointer->procedure return-type
                          (dynamic-func function-name lib)
                          arg-types))))

(define-syntax-rule (define-foreign name return-type func-name arg-types)
  (define-public name
    (fontconfig-func return-type func-name arg-types)))


;;;
;;; Constants
;;;

(define %fc-types
  '(fc-unknown                          ; -1
    fc-void
    fc-integer
    fc-double
    fc-string
    fc-bool
    fc-matrix
    fc-charset
    fc-ftface
    fc-langset
    fc-range))

(define %fc-results
  '(match
    no-match
    type-mismatch
    no-id
    out-of-memory))

(define %fc-match-kinds
  '(pattern font scan))

(define-public (int->fc-type int)
  (list-ref %fc-types (1+ int)))

(define-public (fc-type->int fc-type)
  (1- (list-index (cute eq? <> fc-type) %fc-types)))

(define-public (int->fc-result int)
  (list-ref %fc-results int))

(define-public (fc-result->int fc-result)
  (list-index (cute eq? <> fc-result) %fc-results))

(define-public (int->fc-match-kind int)
  (list-ref %fc-match-kinds int))

(define-public (fc-match-kind->int match-kind)
  (list-index (cute eq? <> match-kind) %fc-match-kinds))

(define-public *attributes*
  '((antialias? "antialias" fc-bool)
    (aspect "aspect" fc-double)
    (char-set "charset" fc-char-set)
    (compatibility "compatibility" fc-string)
    (decorative? "decorative" fc-bool)
    (dpi "dpi" fc-double)
    (embedded-bitmap? "embeddedbitmap" fc-bool)
    (embolden? "embolden" fc-bool)
    (family "family" fc-string)
    (family-lang "familylang" fc-string)
    (file "file" fc-string)
    (font-format "fontformat" fc-string)
    (font-features "fontfeatures" fc-string)
    (font-version "fontversion" fc-integer)
    (foundry "foundry" fc-string)
    (full-name "fullname" fc-string)
    (full-name-lang "fullnamelang" fc-string)
    (hash "hash" fc-string)
    (hinting? "hinting" fc-bool)
    (hint-style "hintstyle" fc-integer)
    (index "index" fc-integer)
    (lcd-filter "lcdfilter" fc-integer)
    (minspace? "minspace" fc-bool)
    (namelang "namelang" fc-string)
    (pixel-size "pixelsize" fc-double)
    (postscriptname "postscriptname" fc-string)
    (prgname "prgname" fc-string)
    (rgba "rgba" fc-integer)
    (outline? "outline" fc-bool)
    (scalable? "scalable" fc-bool)
    (scale "pixelsize" fc-double)
    (size "size" fc-double)
    (spacing "spacing" fc-integer)
    (slant "slant" fc-integer)
    (style "style" fc-string)
    (style-lang "stylelang" fc-string)
    (vertical-layout? "verticallayout" fc-bool)
    (weight "weight" fc-integer)
    (width "width" fc-integer)))

(define-public fc-hintings
  '((none . 0)
    (slight . 1)
    (medium . 2)
    (full . 3)))

(define-public fc-lcd-filters
  '((none . 0)
    (default . 1)
    (light . 2)
    (legacy . 3)))

(define-public fc-slants
  '((roman . 0)
    (italic . 100)
    (oblique . 110)))

(define-public fc-sub-pixel-orders
  '((unknown . 0)
    (rgb . 1)
    (bgr . 2)
    (vrgb . 3)
    (vbgr . 4)
    (none . 5)))

(define-public fc-spacings
  '((proportional . 0)
    (dual . 90)
    (mono . 100)
    (char-cell . 110)))

(define-public fc-weights
  '((thin . 0)
    (extra-light . 40)
    (ultra-light . 40)
    (light . 50)
    (demi-light . 50)
    (semi-light . 50)
    (book . 75)
    (regular . 80)
    (normal . 80)
    (medium . 100)
    (demi-bold . 180)
    (semi-bold . 180)
    (bold . 200)
    (extra-bold . 205)
    (ultra-bold . 205)
    (black . 210)
    (extra-black . 215)
    (ultra-black . 215)))

(define-public fc-widths
  '((ultra-condensed . 50)
    (extra-condensed . 63)
    (consensed . 75)
    (semi-condensed . 87)
    (normal . 100)
    (semi-expanded . 113)
    (expanded . 125)
    (extra-expanded . 150)
    (ultra-expanded . 200)))


;;;
;;; Initialization
;;;

(define-foreign fontconfig-init-load-config-and-fonts
  '* "FcInitLoadConfigAndFonts" '())

(define-foreign fontconfig-finalize
  void "FcFini" '())


;;;
;;; Configuration
;;;

(define-foreign fontconfig-config-destroy
  void "FcConfigDestroy" '(*))


;;;
;;; Version
;;;

(define-foreign fontconfig-get-version
  int "FcGetVersion" '())


;;;
;;; Char Set
;;;

(define-foreign fontconfig-char-set-create
  '* "FcCharSetCreate" '())

(define-foreign fontconfig-char-set-destroy
  void "FcCharSetDestroy" '(*))

(define-foreign fontconfig-char-set-add-char
  int "FcCharSetAddChar" `(* ,uint32))

(define-foreign fontconfig-char-set-has-char
  int "FcCharSetHasChar" `(* ,uint32))


;;;
;;; Font Set
;;;

(define-foreign fontconfig-font-set-create
  '* "FcFontSetCreate" '())

(define-foreign fontconfig-font-set-destroy
  void "FcFontSetDestroy" '(*))

(define-foreign fontconfig-font-set-add
  int "FcFontSetAdd" '(* *))

(define-foreign fontconfig-font-set-match
  '* "FcFontSetMatch" `(* * ,int * *))

(define-foreign fontconfig-font-set-sort
  '* "FcFontSetSort" `(* * ,int * ,int * *))

(define-foreign fontconfig-font-sort
  '* "FcFontSort" `(* * ,int * *))


;;;
;;; Pattern
;;;

(define-foreign fontconfig-pattern-create
  '* "FcPatternCreate" '())

(define-foreign fontconfig-pattern-duplicate
  '* "FcPatternDuplicate" '(*))

(define-foreign fontconfig-pattern-destroy
  void "FcPatternDestroy" '(*))

(define-foreign fontconfig-pattern-equal
  int "FcPatternEqual" '(* *))

(define-foreign fontconfig-pattern-equal-subset
  int "FcPatternEqualSubset" '(* * *))

(define-foreign fontconfig-pattern-add-string
  int "FcPatternAddString" '(* * *))

(define-foreign fontconfig-pattern-add-double
  int "FcPatternAddDouble" `(* * ,double))
(define-foreign fontconfig-pattern-filter
  '* "FcPatternFilter" '(* *))

(define-foreign fontconfig-pattern-add-integer
  int "FcPatternAddInteger" `(* * ,int))

(define-foreign fontconfig-pattern-add-bool
  int "FcPatternAddBool" `(* * ,int))

(define-foreign fontconfig-pattern-add-char-set
  int "FcPatternAddCharSet" '(* * *))

(define-foreign fontconfig-name-parse
  '* "FcNameParse" '(*))

(define-foreign fontconfig-name-unparse
  '* "FcNameUnparse" '(*))

(define-foreign fontconfig-pattern-format
  '* "FcPatternFormat" '(* *))

(define-foreign fontconfig-pattern-print
  void "FcPatternPrint" '(*))

;; FcResult (FcPattern *p, const char *object, int id, FcValue *v);
(define-foreign fontconfig-pattern-get
  int "FcPatternGet" `(* * ,int *))

(define-foreign fontconfig-pattern-get-integer
  int "FcPatternGetInteger" `(* * ,int *))

(define-foreign fontconfig-pattern-get-double
  int "FcPatternGetDouble" `(* * ,int *))

(define-foreign fontconfig-pattern-get-bool
  int "FcPatternGetBool" `(* * ,int *))

(define-foreign fontconfig-pattern-get-char-set
  int "FcPatternGetCharSet" `(* * ,int *))

(define-foreign fontconfig-pattern-get-string
  int "FcPatternGetString" `(* * ,int *))

(define-foreign fontconfig-config-substitute
  int "FcConfigSubstitute" `(* * ,int))

(define-foreign fontconfig-pattern-delete
  int "FcPatternDel" '(* *))

(define-foreign fontconfig-pattern-remove
  int "FcPatternRemove" `(* * ,int))

(define-foreign fontconfig-default-substitute
  void "FcDefaultSubstitute" '(*))

(define-foreign fontconfig-font-match
  '* "FcFontMatch" `(* * *))

(define-foreign fontconfig-font-render-prepare
  '* "FcFontRenderPrepare" `(* * *))

;;;
;;; Object Set
;;;
(define-foreign fontconfig-object-set-create
  '* "FcObjectSetCreate" '())

(define-foreign fontconfig-object-set-add
  int "FcObjectSetAdd" '(* *))

(define-foreign fontconfig-font-list
  '* "FcFontList" '(* * *))


;;;
;;; Utils
;;;

(define-foreign fontconfig-str-free
  void "FcStrFree" '(*))

(define-foreign fontconfig-object-set-destroy
  void "FcObjectSetDestroy" '(*))
