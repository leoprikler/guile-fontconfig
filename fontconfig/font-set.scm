;;; guile-fontconfig --- FFI bindings for FontConfig
;;; Copyright © 2021 Leo Prikler <leo.prikler@student.tugraz.at>
;;;
;;; This file is part of guile-fontconfig.
;;;
;;; Guile-fontconfig is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU Lesser General Public License
;;; as published by the Free Software Foundation; either version 3 of
;;; the License, or (at your option) any later version.
;;;
;;; Guile-fontconfig is distributed in the hope that it will be
;;; useful, but WITHOUT ANY WARRANTY; without even the implied
;;; warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
;;; See the GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU Lesser General Public
;;; License along with guile-fontconfig.  If not, see
;;; <http://www.gnu.org/licenses/>.

(define-module (fontconfig font-set)
  #:use-module ((fontconfig bindings) #:prefix ffi:)
  #:use-module (fontconfig char-set)
  #:use-module (fontconfig globals)
  #:use-module (fontconfig object-set)
  #:use-module (fontconfig pattern)
  #:use-module ((oop goops) #:select (make is-a?))
  #:use-module (ice-9 match)
  #:use-module (rnrs bytevectors)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-26)
  #:use-module (system foreign)
  #:use-module (system foreign-object)
  #:export (font-set-add!
            font-list font-set-match
            font-sort font-set-sort
            font-set->list list->font-set))

(define %unwrapped-font-sets (make-weak-value-hash-table))

(define (font-set-finalize! font-set)
  (ffi:fontconfig-font-set-destroy
   (%unwrap-font-set font-set)))

(define-foreign-object-type <fc:font-set>
  %make-font-set (font-set-ptr)
  #:finalizer font-set-finalize!)

(define (%unwrap-font-set font-set)
  (make-pointer (font-set-ptr font-set)))

(define (unwrap-font-set font-set)
  (or (hashq-ref %unwrapped-font-sets font-set)
      (let ((p (%unwrap-font-set font-set)))
        (hashq-set! %unwrapped-font-sets font-set p)
        p)))

(define (wrap-font-set ptr)
  (%make-font-set (pointer-address ptr)))

(define font-set? (cute is-a? <> <fc:font-set>))

;; Add a @var{pattern} to @var{font-set}.
(define (font-set-add! font-set pattern)
  (unless (eq?
           1
           (ffi:fontconfig-font-set-add
            (unwrap-font-set font-set) (unwrap-pattern pattern)))
    (scm-error 'memory-allocation-error
               "font-set-add!" "failed to add pattern to font-set" '() #f)))

(define (font-set->list font-set)
  (match (parse-c-struct (unwrap-font-set font-set) (list int int '*))
    ((0 _ _) '())
    ((nfont _ fonts)
     (let ((fontv (pointer->bytevector fonts (* nfont (sizeof '*)))))
       (map
        (compose wrap-pattern
                 dereference-pointer
                 (cute bytevector->pointer fontv <>))
        (iota nfont 0 (sizeof '*)))))))

(define (list->font-set list)
  (let ((font-set (wrap-font-set (ffi:fontconfig-font-set-create))))
    (when (= 0 (font-set-ptr font-set))
      (scm-error 'memory-allocation-error
                 "list->font-set" "failed to allocate font-set" '() #f))
    (for-each (cute font-set-add! font-set <>) list)
    font-set))

(define (font-list pattern os)
  (wrap-font-set
   (ffi:fontconfig-font-list
    %null-pointer (unwrap-pattern pattern) (unwrap-object-set os))))

(define (%font-set-match sets pattern)
  (let* ((nsets (length sets))
         (setv (make-bytevector (* nsets (sizeof '*))))
         (result (make-bytevector (sizeof int))))
    (for-each
     (cute bytevector-sint-set! setv <> <> (native-endianness) (sizeof '*))
     (iota nsets 0 (sizeof '*))
     (map (compose pointer-address unwrap-font-set) sets))
    (let* ((%pat (ffi:fontconfig-font-set-match (unwrap-config (*fontconfig*))
                                                (bytevector->pointer setv)
                                                nsets
                                                (unwrap-pattern pattern)
                                                (bytevector->pointer result)))
           (result (ffi:int->fc-result (bytevector-s32-native-ref result 0))))
      (match result
        ('match (wrap-pattern %pat))
        ('no-match #f)
        ('type-mismatch (error "type mismatch"))
        (_ (error "unexpected result" result))))))

(define (font-set-match set-or-sets pattern)
  (%font-set-match (if (font-set? set-or-sets)
                       (list set-or-sets)
                       set-or-sets)
                   pattern))

(define (%font-set-sort sets pattern trim?)
  (let* ((nsets (length sets))
         (setv (make-bytevector (* nsets (sizeof '*))))
         (%csp (make-bytevector (sizeof '*)))
         (csp (bytevector->pointer %csp))
         (result (make-bytevector (sizeof int))))
    (for-each
     (cute bytevector-sint-set! setv <> <> (native-endianness) (sizeof '*))
     (iota nsets 0 (sizeof '*))
     (map (compose pointer-address unwrap-font-set) sets))
    (let* ((%fonts (ffi:fontconfig-font-set-sort (unwrap-config (*fontconfig*))
                                                 (bytevector->pointer setv)
                                                 nsets
                                                 (unwrap-pattern pattern)
                                                 (if trim? 1 0)
                                                 csp
                                                 (bytevector->pointer result)))
           (result (ffi:int->fc-result (bytevector-s32-native-ref result 0))))
      (match result
        ('match (values (wrap-font-set %fonts)
                        (fc-char-set->char-set
                         (wrap-char-set (dereference-pointer csp)))))
        ('no-match (values #f char-set:empty))
        ('type-mismatch (error "type mismatch"))
        (_ (error "unexpected result" result))))))

(define* (font-set-sort set-or-sets pattern #:optional (trim? #t))
  (%font-set-sort (if (font-set? set-or-sets)
                      (list set-or-sets)
                      set-or-sets)
                  pattern
                  trim?))

(define* (font-sort pattern #:optional (trim? #t))
  (let* ((%csp (make-bytevector (sizeof '*)))
         (csp (bytevector->pointer %csp))
         (result (make-bytevector (sizeof int)))
         (%fonts (ffi:fontconfig-font-sort (unwrap-config (*fontconfig*))
                                           (unwrap-pattern pattern)
                                           (if trim? 1 0)
                                           csp
                                           (bytevector->pointer result)))
         (result (ffi:int->fc-result (bytevector-s32-native-ref result 0))))
    (match result
      ('match (values (wrap-font-set %fonts)
                      (fc-char-set->char-set
                       (wrap-char-set (dereference-pointer csp)))))
      ('no-match (values #f char-set:empty))
      ('type-mismatch (error "type mismatch"))
      (_ (error "unexpected result" result)))))
