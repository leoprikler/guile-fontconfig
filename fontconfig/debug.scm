;;; guile-fontconfig --- FFI bindings for FontConfig
;;; Copyright © 2021 Leo Prikler <leo.prikler@student.tugraz.at>
;;;
;;; This file is part of guile-fontconfig.
;;;
;;; Guile-fontconfig is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU Lesser General Public License
;;; as published by the Free Software Foundation; either version 3 of
;;; the License, or (at your option) any later version.
;;;
;;; Guile-fontconfig is distributed in the hope that it will be
;;; useful, but WITHOUT ANY WARRANTY; without even the implied
;;; warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
;;; See the GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU Lesser General Public
;;; License along with guile-fontconfig.  If not, see
;;; <http://www.gnu.org/licenses/>.

(define-module (fontconfig debug)
  #:use-module ((fontconfig bindings) #:prefix ffi:)
  #:use-module (fontconfig pattern)
  #:export (print-pattern))

(define (print-pattern pattern)
  (ffi:fontconfig-pattern-print (unwrap-pattern pattern)))
