;;; guile-fontconfig --- FFI bindings for FontConfig
;;; Copyright © 2021 Leo Prikler <leo.prikler@student.tugraz.at>
;;;
;;; This file is part of guile-fontconfig.
;;;
;;; Guile-fontconfig is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU Lesser General Public License
;;; as published by the Free Software Foundation; either version 3 of
;;; the License, or (at your option) any later version.
;;;
;;; Guile-fontconfig is distributed in the hope that it will be
;;; useful, but WITHOUT ANY WARRANTY; without even the implied
;;; warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
;;; See the GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU Lesser General Public
;;; License along with guile-fontconfig.  If not, see
;;; <http://www.gnu.org/licenses/>.

(define-module (fontconfig char-set)
  #:use-module ((fontconfig bindings) #:prefix ffi:)
  #:use-module (rnrs bytevectors)
  #:use-module (system foreign)
  #:use-module (system foreign-object)
  #:export (wrap-char-set
            unwrap-char-set
            char-set->fc-char-set
            fc-char-set->char-set))

(define (char-set-finalize! char-set)
  (ffi:fontconfig-char-set-destroy
   (unwrap-char-set char-set)))

(define-foreign-object-type <fc:char-set>
  %make-char-set (char-set-ptr)
  #:finalizer char-set-finalize!)

(define (unwrap-char-set char-set)
  (make-pointer (char-set-ptr char-set)))

(define (wrap-char-set ptr)
  (%make-char-set (pointer-address ptr)))

(define (%char-set-add! %char-set char)
  (unless (= 1 (ffi:fontconfig-char-set-add-char
                %char-set
                (char->integer char)))
    (scm-error 'memory-allocation-error
               "char-set-add!" "failed to add char to char-set" '() #f)))

(define (%char-set-has-char? %char-set char)
  (= 1 (ffi:fontconfig-char-set-has-char
        %char-set
        (char->integer char))))

(define (char-set->fc-char-set char-set)
  (wrap-char-set
   (char-set-fold (lambda (char seed)
                    (%char-set-add! seed char)
                    seed)
                  (ffi:fontconfig-char-set-create)
                  char-set)))

(define (fc-char-set->char-set fc-char-set)
  (let ((%cs (unwrap-char-set fc-char-set)))
    (char-set-filter (lambda (c) (%char-set-has-char? %cs c)) char-set:full)))
