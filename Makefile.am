# guile-fontconfig --- FFI bindings for FontConfig
# Copyright © 2021 Nicolò Balzarotti <nicolo@nixo.xyz>
#
# This file is part of guile-fontconfig.
#
# Guile-fontconfig is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public License
# as published by the Free Software Foundation; either version 3 of
# the License, or (at your option) any later version.
#
# Guile-fontconfig is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with guile-fontconfig.  If not, see
# <http://www.gnu.org/licenses/>.

GOBJECTS = $(SOURCES:%.scm=%.go)

nobase_mod_DATA = $(SOURCES) $(NOCOMP_SOURCES)
nobase_go_DATA = $(GOBJECTS)

# Make sure source files are installed first, so that the mtime of
# installed compiled files is greater than that of installed source
# files.  See
# <http://lists.gnu.org/archive/html/guile-devel/2010-07/msg00125.html>
# for details.
guile_install_go_files = install-nobase_goDATA
$(guile_install_go_files): install-nobase_modDATA

CLEANFILES = $(GOBJECTS)
EXTRA_DIST = $(SOURCES) $(NOCOMP_SOURCES) $(TESTS)
GUILE_WARNINGS = -Wunbound-variable -Warity-mismatch -Wformat \
  -Wmacro-use-before-definition \
  -Wbad-case-datum -Wduplicate-case-datum

SUFFIXES = .scm .go
.scm.go:
	$(AM_V_GEN)$(top_builddir)/pre-inst-env $(GUILE_TOOLS) compile --target="$(host)" $(GUILE_WARNINGS) -o "$@" "$<"

doc/snarf/%.texi: %.scm scripts/doc-snarf.go
	@mkdir -p $$(dirname "$@")
	$(AM_V_GEN)$(top_builddir)/pre-inst-env $(GUILE_TOOLS) doc-snarf --lang=scheme --texinfo -o "$@" "$<"
	@sed -i -e "//d" "$@"

moddir=$(prefix)/share/guile/site/$(GUILE_EFFECTIVE_VERSION)
godir=$(libdir)/guile/$(GUILE_EFFECTIVE_VERSION)/site-ccache

SOURCES =					\
  fontconfig.scm				\
  fontconfig/bindings.scm			\
  fontconfig/char-set.scm			\
  fontconfig/config.scm				\
  fontconfig/debug.scm				\
  fontconfig/font-set.scm			\
  fontconfig/globals.scm			\
  fontconfig/object-set.scm			\
  fontconfig/pattern.scm

EXAMPLES = \
  examples/example.scm \
  examples/match.scm

EXTRA_DIST +=					\
  pre-inst-env.in				\
  README					\
  README.org					\
  guix.scm					\
  $(EXAMPLES)					\
  build-aux/test-driver.scm			\
  doc/fdl-1.3.texi				\
  scripts/doc-snarf.scm

info_TEXINFOS = doc/guile-fontconfig.texi
doc_guile_fontconfig_TEXINFOS = $(SOURCES:%.scm=doc/snarf/%.texi)

SCM_LOG_DRIVER = \
  $(top_builddir)/pre-inst-env \
  $(GUILE) --no-auto-compile -e main $(top_srcdir)/build-aux/test-driver.scm

TEST_EXTENSIONS = .scm

TESTS = \
  test/char-set.scm \
  test/font-set.scm \
  test/pattern.scm

clean-local:
	-rm -rf doc/snarf scripts/doc-snarf.go

DISTCLEANFILES = \
  doc/guile-fontconfig.info

.SECONDARY: scripts/doc-snarf.go
